# Quora Questions Analysis

## Is It Possible to Differentiate Between Sincere Questions and Insincere Questions?

<br>

### Study Introduction

The aim of this project consists in building and training a model that allow us to evaluate questions from users in the Quora platform as either Sincere or Insincere.

The Dataset was created by Quora's team. They also classified the questions as either Sincere or Insincere.

I've chosen to build two Naive Bayes Classifier Models from scratch to train and test the model, one for Stemmed Words, and other for Not Stemmed Words (or, more succinctly, normal words). In the end of the project, I've compared my results with a classifier provided by Scikit-learn.

<br>

### Model Interpretation

The differences between models are not significant, for this approach, a stemmed or not stemmed version seems to be indifferent. Although probably not statistically significant, the Stemmed Model tends to more correctly classify questions as Insincere, and the Not Stemmed Model appears to be better at correctly classifying Sincere questions.

The accuracy is indeed high, but this comes from the fact that the data is unbalanced; as there are not many Insincere questions, since the model assumes a Sincere question by default, it is normal that most question will be correctly classified.

However, the other metrics show the not so good performance in differentiating questions. Both Recall and Precision (and, consequently, the F-Score) are low. This comes from the fact that the model has a small percentage of True Positives; many Insincere questions pass through the filter, and some Sincere questions are wrongly classified as Insincere.

As such, I'm concluding that this type of analysis is not suited to differentiate Sincere and Insincere questions. I'm assuming two reasons for this to happen, first, the sincerity of a question is probably best analyzed not from the words it is using, but from the meaning they are trying to convey, second, it could be that the classification of the questions was not done in the best way possible, and, as such, is introducing a bias into the model.

It could also be interesting to train a model with a more balanced dataset.

<br>

##### All data is archived in a .7z file, as the original file are very big and wouldn't otherwise be able to be pushed into Github.

##### This was done to better understand Udemy's Complete 2020 Data Science & Machine Learning Bootcamp, Sections 6, 7 and 8.

##### Use nbviwer, pasting this repo's notebook link, if Gitlab servers are down;
##### nbviwer website -> https://nbviewer.jupyter.org/
